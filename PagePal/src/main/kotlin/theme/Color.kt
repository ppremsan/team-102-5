package theme

import androidx.compose.ui.graphics.Color

val grey = Color(0xFF404040)
val darkblue = Color(0xFF293040)
val green = Color(0xFF5FD855)
val lightgrey = Color(0xFFC1C1C1)
val lightbrown = Color(0xFFC9AD8B)
val whitevariation = Color(0xFFFAFAFA)